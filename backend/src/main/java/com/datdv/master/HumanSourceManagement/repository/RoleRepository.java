package com.datdv.master.HumanSourceManagement.repository;

import com.datdv.master.HumanSourceManagement.common.ERole;
import com.datdv.master.HumanSourceManagement.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);
}
