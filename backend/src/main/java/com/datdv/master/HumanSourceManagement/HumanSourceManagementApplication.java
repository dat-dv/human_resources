package com.datdv.master.HumanSourceManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HumanSourceManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(HumanSourceManagementApplication.class, args);
	}

}
