package com.datdv.master.HumanSourceManagement.dto;

import lombok.Data;

import java.util.Set;

@Data
public class SignUpRequest {
    private String name;
    private String email;
    private String password;
    private String phone;
    private String address;
    private Set<String> role;


}
