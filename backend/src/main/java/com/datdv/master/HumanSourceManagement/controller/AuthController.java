package com.datdv.master.HumanSourceManagement.controller;

import com.datdv.master.HumanSourceManagement.common.ERole;
import com.datdv.master.HumanSourceManagement.common.JwtUtils;
import com.datdv.master.HumanSourceManagement.dto.JwtResponse;
import com.datdv.master.HumanSourceManagement.dto.LoginRequest;
import com.datdv.master.HumanSourceManagement.dto.MessageResponse;
import com.datdv.master.HumanSourceManagement.dto.SignUpRequest;
import com.datdv.master.HumanSourceManagement.entity.Role;
import com.datdv.master.HumanSourceManagement.entity.User;
import com.datdv.master.HumanSourceManagement.repository.RoleRepository;
import com.datdv.master.HumanSourceManagement.repository.UserRepository;
import com.datdv.master.HumanSourceManagement.service.impl.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired AuthenticationManager authenticationManager;

  @Autowired UserRepository userRepository;
  @Autowired RoleRepository roleRepository;

  @Autowired PasswordEncoder passwordEncoder;
  @Autowired JwtUtils jwtUtils;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticate(@Validated @RequestBody LoginRequest LoginRequest) {
    Authentication authentication =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                LoginRequest.getEmail(), LoginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

    List<String> roles =
        userDetails.getAuthorities().stream()
            .map(item -> item.getAuthority())
            .collect(Collectors.toList());

    return ResponseEntity.ok(
        new JwtResponse(jwt, userDetails.getId(), userDetails.getEmail(), roles));
  }

  @PostMapping("/signup")
  public ResponseEntity<?> register(@Validated @RequestBody SignUpRequest request) {
    if (userRepository.existsByEmail(request.getEmail())) {
      return ResponseEntity.badRequest().body(new MessageResponse("Error: email has been taken"));
    }

    User user =
        new User(
            request.getName(),
            request.getEmail(),
            request.getAddress(),
            request.getPhone(),
            passwordEncoder.encode(request.getPassword()));

    Set<String> strRoles = request.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role devRole =
          roleRepository
              .findByName(ERole.ROLE_DEVELOPER)
              .orElseThrow(() -> new RuntimeException("Role is not found"));
      roles.add(devRole);
    } else {
      strRoles.forEach(
          role -> {
            switch (role) {
              case "admin":
                Role adminRole =
                    roleRepository
                        .findByName(ERole.ROLE_ADMIN)
                        .orElseThrow(() -> new RuntimeException("Role is not found"));
                roles.add(adminRole);
                break;
              case "accounts":
                Role accountantRole =
                    roleRepository
                        .findByName(ERole.ROLE_ACCOUNTANT)
                        .orElseThrow(() -> new RuntimeException("Role is not found"));
                roles.add(accountantRole);
                break;
              case "business":
                Role businessRole =
                    roleRepository
                        .findByName(ERole.ROLE_BUSINESSMAN)
                        .orElseThrow(() -> new RuntimeException("Role is not found"));
                roles.add(businessRole);
                break;
              case "hr":
                Role hrRole =
                    roleRepository
                        .findByName(ERole.ROLE_HR)
                        .orElseThrow(() -> new RuntimeException("Role is not found"));
                roles.add(hrRole);
                break;
              default:
                Role devRole =
                    roleRepository
                        .findByName(ERole.ROLE_DEVELOPER)
                        .orElseThrow(() -> new RuntimeException("Role is not found"));
                roles.add(devRole);
                break;
            }
          });
    }
    user.setRoles(roles);
    userRepository.save(user);
    return ResponseEntity.ok(new MessageResponse("User registered successfully"));
  }
}
