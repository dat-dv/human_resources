package com.datdv.master.HumanSourceManagement.service.impl;

import com.datdv.master.HumanSourceManagement.dto.UserDto;
import com.datdv.master.HumanSourceManagement.entity.User;
import com.datdv.master.HumanSourceManagement.repository.UserRepository;
import com.datdv.master.HumanSourceManagement.service.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserServiceImpl implements UserService {
  private final UserRepository userRepository;
  private final ModelMapper mapper;
  private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

  @Autowired
  public UserServiceImpl(UserRepository userRepository, ModelMapper mapper) {
    this.userRepository = userRepository;
    this.mapper = mapper;
  }

  @Override
  public List<UserDto> getListUsers() {
    log.info("All users; {}", userRepository.findAll());
    return userRepository.findAll().stream()
        .map(user -> mapper.map(user, UserDto.class))
        .collect(Collectors.toList());
  }
}
