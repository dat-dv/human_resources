package com.datdv.master.HumanSourceManagement.entity;

import com.datdv.master.HumanSourceManagement.common.ERole;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "role")
public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column
  private ERole name;
}
