package com.datdv.master.HumanSourceManagement.dto;

import lombok.Data;

@Data
public class UserDto {
  private Long id;

  private String name;

  private String email;

  private String address;

  private String phone;

  private String role;
}
