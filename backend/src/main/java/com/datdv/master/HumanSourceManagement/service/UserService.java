package com.datdv.master.HumanSourceManagement.service;

import java.util.List;
import java.util.Optional;

import com.datdv.master.HumanSourceManagement.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    List<UserDto> getListUsers();
}
