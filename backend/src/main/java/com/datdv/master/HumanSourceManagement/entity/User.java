package com.datdv.master.HumanSourceManagement.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "user")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long Id;

  @Column private String name;

  @Column private String email;

  @Column private String address;

  @Column private String phone;

  @Column private String password;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "user_role",
      joinColumns = @JoinColumn(name = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "role_id"))
  private Set<Role> roles = new HashSet<>();

  public User(String name, String email, String address, String phone, String password) {
    this.name = name;
    this.email = email;
    this.address = address;
    this.phone = phone;
    this.password = password;
  }

  public User() {
  }
}
