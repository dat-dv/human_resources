package com.datdv.master.HumanSourceManagement.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {

  @GetMapping("/all")
  public String allAccess() {
    return "Public Content.";
  }

  @GetMapping("/user")
  @PreAuthorize(
      "hasRole('DEVELOPER') or hasRole('ACCOUNTANT') or hasRole('ADMIN') or hasRole('BUSINESSMAN') or hasRole('HR')")
  public String userAccess() {
    return "User Content.";
  }

  @GetMapping("/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public String adminAccess() {
    return "admin Board.";
  }

  @GetMapping("/developer")
  @PreAuthorize("hasRole('DEVELOPER')")
  public String developerAccess() {
    return "developer Board.";
  }

  @GetMapping("/hr")
  @PreAuthorize("hasRole('HR')")
  public String hrAccess() {
    return "hr Board.";
  }
}
