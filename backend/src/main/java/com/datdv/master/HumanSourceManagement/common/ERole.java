package com.datdv.master.HumanSourceManagement.common;

public enum ERole {
  ROLE_DEVELOPER,
  ROLE_ADMIN,
  ROLE_HR,
  ROLE_ACCOUNTANT,
  ROLE_BUSINESSMAN
}
