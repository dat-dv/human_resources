package com.datdv.master.HumanSourceManagement.repository;

import com.datdv.master.HumanSourceManagement.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  /**
   * Find user by email
   *
   * @param email email of user
   * @return User
   */
  Optional<User> findByEmail(String email);


  Boolean existsByEmail(String email);
}
