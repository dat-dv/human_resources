package com.datdv.master.HumanSourceManagement.controller;

import com.datdv.master.HumanSourceManagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired private UserService userService;

  @GetMapping()
  public ResponseEntity<?> getListUser() {
    return ResponseEntity.ok(userService.getListUsers());
  }

  @PostMapping()
  public ResponseEntity<?> createUser() {
    return null;
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> updateUser(@PathVariable String id) {
    return null;
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteUser(@PathVariable String id) {
    return null;
  }
}
